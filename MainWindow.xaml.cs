﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace ImageCutter
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region Init
        public MainWindow()
        {
            InitializeComponent();
            Log = new ObservableCollection<string>();
            DataContext = this;
        }
        private class TileItem
        {
            public string Name { get; set; }
            public int Width { get; set; }
            public int Height { get; set; }
            public int X { get; set; }
            public int Y { get; set; }

        }
        #endregion

        #region Methods
        private void CutWithXML(string source)
        {
            List<TileItem> items = new List<TileItem>();
            XmlDocument doc = new XmlDocument();
            doc.Load(source);
            var root = doc.FirstChild;
            var path = source.Substring(0,source.LastIndexOf('\\'));
            var aaa = root.Attributes.GetNamedItem("imagePath").Value;
            Bitmap bmp = new Bitmap(path+'\\'+aaa);

            XmlNode child = root.FirstChild;
            while (child != root.LastChild)
            {
                TileItem item = new TileItem();
                item.Name = child.Attributes.GetNamedItem("name").Value;
                item.Width = Convert.ToInt32(child.Attributes.GetNamedItem("width").Value);
                item.Height = Convert.ToInt32(child.Attributes.GetNamedItem("height").Value);
                item.X = Convert.ToInt32(child.Attributes.GetNamedItem("x").Value);
                item.Y = Convert.ToInt32(child.Attributes.GetNamedItem("y").Value);
                items.Add(item);
                child = child.NextSibling;
            }
            Directory.CreateDirectory(path+"\\out");
            foreach(var a in items)
            {
                Bitmap cuted = bmp.Clone(new System.Drawing.Rectangle(a.X, a.Y, a.Width, a.Height), bmp.PixelFormat);
                cuted.Save(path + "\\out\\" + a.Name);
            }
        }
        private void CutWithSettings(string source)
        {
            try
            {

                Dispatcher.Invoke(() => Log.Clear());
                Dispatcher.Invoke(() => Log.Add("Source: " + source));
                string path = source.Substring(0, source.Length - 4);
                Dispatcher.Invoke(() => Log.Add("Path: " + path));
                if (Directory.Exists(path))
                {
                    Dispatcher.Invoke(() => Log.Add("Deleting directory ..."));
                    System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(path);

                    foreach (FileInfo file in downloadedMessageInfo.GetFiles())
                    {
                        file.Delete();
                    }
                    foreach (DirectoryInfo dir in downloadedMessageInfo.GetDirectories())
                    {
                        dir.Delete(true);
                    }
                    Directory.Delete(path);
                    Dispatcher.Invoke(() => Log.Add("Directory deleted ..."));
                }
                Directory.CreateDirectory(path);
                Dispatcher.Invoke(() => Log.Add("Directory created ..."));
                Bitmap bmp = new Bitmap(source);
                Dispatcher.Invoke(() => Log.Add("Start cutting ..."));
                Dispatcher.Invoke(() => pBar.Maximum = (bmp.Height / (TileHeight + Spacing)) * (bmp.Width / (TileWidth + Spacing)));
                for (int y = TileMargin; y <= bmp.Height; y += TileHeight + Spacing)
                {
                    for (int x = TileMargin; x <= bmp.Width; x += TileWidth + Spacing)
                    {
                        Bitmap cuted = bmp.Clone(new System.Drawing.Rectangle(x, y, TileWidth, TileHeight), bmp.PixelFormat);
                        bool flag = false;
                        for (int xx = 0; xx < cuted.Width; xx++)
                        {
                            for (int yy = 0; yy < cuted.Height; yy++)
                            {
                                if (flag) break;
                                if (cuted.GetPixel(xx, yy).A != 0)
                                {
                                    flag = true;
                                    break;
                                }
                            }
                        }
                        if (flag)
                            cuted.Save(path + "\\tile" + y / (TileHeight + Spacing) + 1 + "_" + x / (TileWidth + Spacing) + 1 + ".png", ImageFormat.Png);
                        Dispatcher.Invoke(() => pBar.Value +=1);
                    }

                }
                Dispatcher.Invoke(() => Log.Add("All OK."));
            }
            catch (Exception ex)
            {
                Dispatcher.Invoke(() => Log.Add(ex.Message));
            }
        }
        #endregion

        #region Properties
        private int _tileMargin = 0;
        private int _tileWidth = 15;
        private int _tileHieght = 15;
        private int _spacing = 2;
        public int TileMargin
        {
            get { return _tileMargin; }
            set
            {
                _tileMargin = value;
                OnPropertyChanged("Margin");
            }
        }
        public int TileWidth
        {
            get { return _tileWidth; }
            set
            {
                _tileWidth = value;
                OnPropertyChanged("Width");
            }
        }
        public int TileHeight
        {
            get { return _tileHieght; }
            set
            {
                _tileHieght = value;
                OnPropertyChanged("Height");
            }
        }
        public int Spacing
        {
            get { return _spacing; }
            set
            {
                _spacing = value;
                OnPropertyChanged("Spacing");
            }
        }
        public ObservableCollection<string> Log { get; set; }
        #endregion

        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(params string[] property)
        {
            if (PropertyChanged != null)
                foreach (var prop in property)
                    PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
        #endregion

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Image Files(*.BMP;*.JPG;*.PNG)|*.BMP;*.JPG;*.GIF|XML Spritesheet(*.XML)|*.XML|All files (*.*)|*.*";
            dlg.FilterIndex = 2;
            if (dlg.ShowDialog() ?? false)
            {
                switch (dlg.FilterIndex)
                {
                    case 2:
                        pBar.Value = 0;
                        new Thread(() => CutWithXML(dlg.FileName)).Start();
                        break;
                    default:
                        pBar.Value = 0;
                        new Thread(() => CutWithSettings(dlg.FileName)).Start();
                        break;
                }
            }
        }
    }
}
